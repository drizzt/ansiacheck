CREATE TABLE `news` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `link` varchar(255) UNIQUE NOT NULL,
  `json` text NOT NULL,
  `hash` varchar(100) NOT NULL,
  `ots` text NOT NULL,
  `ots_status` int(11) NOT NULL DEFAULT '0'
);
